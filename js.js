"use strict";
let ul = document.createElement('ul');

let body = document.querySelector('body');
fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(data => {
        data.forEach(({ episodeId, name, openingCrawl, characters }) => {
            let div = document.createElement('div');
            div.innerHTML = `<div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>`
            div.className = 'lds-default';
            let li = document.createElement('li');
            li.innerHTML = `Episod: ${episodeId};
                        Film title: ${name};
                        Film synopsis: ${openingCrawl}`;
            li.append(div);
            ul.append(li);

            let arrChar = characters.map((url) => fetch(url).then(res => res.json()));
            Promise.allSettled(arrChar)
            .then(data => {
                let names = data.map(({ value }) => value.name);
                let p = document.createElement('p');
                p.innerText = `Actors: ${names.join(', ')}`;
                div.classList.remove('lds-default');
                li.append(p);
            })
        })
    })
body.append(ul);

